# Web-based Systems

This project was part of the lecture **Web-based Systems** at TU Braunschweig

Processing period: Apr. 2018 - Aug. 2018



## Deployment
![Deployment](documentation/Deployment.png)

## Usage

### [NPM](https://docs.npmjs.com)

``` bash
npm install package
npm install
npm start
```
### [Docker-Compose](https://docs.docker.com/compose/overview/)

``` bash
docker-compose build
docker-compose start
docker-compose up
docker-compose down
docker-compose kill
```
