const express = require('express');
const router = express.Router();
const mongo = require('../public/javascript/mongo.js');
const MongoClient = require('mongodb').MongoClient;
const app = express();

 // Connection URL
 const url = 'mongodb://134.169.47.82:27017';
 const dbName = 'myproject';

/**
 * GIF LIMIT
 * used in Gif Query
 * @type {number}
 */
const GIF_LIMIT = 10;

/**
 * Gif API
 */
var GphApiClient = require('giphy-js-sdk-core');
client = GphApiClient("dc6zaTOxFJmzC");

/**
 * Routes
 */
 router.get('/', (req, res, next) => {
	 
	res.render('chat', {title: 'Web-Sys-Chat'});
	res.end();

});

//Insert posted data to MongoDB
router.post('/newMessage', function(req, res, next) {
	console.log('Request Body:'); 	
	console.log(req.body); 	
	const dateTime = Date.now();
	const timestamp = Math.floor(dateTime / 1000);
	
	var data = [];
	data.push({type: req.body.type, sender: req.body.sender, text: req.body.text, timestamp: timestamp});
	console.log('Request with timestamp');
	console.log(data[0]);
	//Insert
	mongo.insert(data[0]);
    res.writeHead(301, {'Location':  './messages'});//?
    res.end();
	

});

//Laden der Daten
router.get('/messages', function(req, res, next) {
	//READ FROM DB
	mongo.find((found)=> {
		var resultAsString = JSON.stringify(found);
		res.write(resultAsString);
		res.end();
	});
	
});

//Laden der Daten
router.get('/delete', function(req, res, next) {
	//READ FROM DB
	mongo.delete();
	res.writeHead(301, {'Location': '../chat'});
	res.end();	
});


router.get('/gif', function (req, res, next) {
    let fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
	let url = new URL(fullUrl);
    let searchParams = new URLSearchParams(url.search);

    client.search('gifs', {"q": searchParams.get('text'), "limit": GIF_LIMIT})
        .then((response) => {
            var clientResponse = [];
            response.data.forEach((gifObject) => {
                clientResponse.push({id: gifObject.images.media_id, img_low_quality: gifObject.images.fixed_height_still.gif_url, img_high_quality: gifObject.images.original.gif_url});
                //AND SEND ID
            })
            var jsonResponse = JSON.stringify(clientResponse);
            console.log(jsonResponse);
            res.write(jsonResponse);
            res.end();
        })
        .catch((err) => {
            console.log(err);
        })
});

module.exports = router;
