const express = require('express');
const router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Web-Sys Chat', name: 'Moritz' });//index.pug, args
  //res.writeHead(301, {'Location':  './chat'});//?
  res.end();
});

router.post('/', function(req, res, next) {
  console.log('INDEX POST');
  res.write('POST in index /');
  res.end();
  
});


module.exports = router;
