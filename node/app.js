//MODULES
const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const app = require('express')();
const http = require('http');
const chat = require('./routes/chat.js');

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * view engine setup
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));//../03_jquery_client/client

/**
 * Define routes
 */
//app.use('/', indexRouter);
app.use('/chat', chat);


/**
 * Socket Connection
 */
var io = require('socket.io')(server);
io.on('connection', function(socket){
    console.log('a user connected');

    socket.on('newMessage', (msg) => {
        console.log('Neue Nachricht empfangen: ' + msg);
        io.emit('newMessage', msg);
    });

    socket.on('disconnect', function(){
        console.log('user disconnected');
    });

    socket.on('typing', function() {
        io.emit('typing');
    });

    socket.on('noLongerTpying', function() {
        io.emit('noLongerTpying');
    });

    socket.on('webRtcRequest', function(data) {
      io.emit('webRtcRequest', data);
      console.log("Data");
      console.log(data);
    });

});


// catch 404 and forward to error handler
app.use(function(req, res, next) {

	next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
	console.log(err);
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', { error: err });
});

module.exports = app;
module.exports.server = server;
