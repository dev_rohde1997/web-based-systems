var Peer = require('simple-peer');
const ENTER_KEY_VALUE = 13;
var webRtcPeer;
var socket = io();


socket.on('webRtcRequest',(data) => onWebRtcRequest(data));


$(document).ready(function () {
    sendWebRtcRequest().then(function () {
        $('#fileInput').change(readFile);
    })
});

function onWebRtcRequest(received) {
    console.log('Received');
    console.log(received);
    var username = received.username;
    var payload = JSON.parse(received.payload);
    if(username === localStorage.getItem("username")) {
        return;
    } else if (payload.type === 'offer') {
        webRtcPeer = new Peer({initiator: false, tickle: false, objectMode: true});
        webRtcPeer.signal(received.payload);
        webRtcPeer.on('signal', function (data) {
            if (data.type === 'offer') {
                console.log('New Offer - Non initiator');
                webRtcPeer.signal(data);
            } else if (data.type === 'answer'){
                console.log('Answer received - Non initiator');
                socket.emit('webRtcRequest', {'username': localStorage.getItem("username"), 'payload': JSON.stringify(data)});
            }

        });

        webRtcPeer.on('data', function (data) {
            console.log('got a message from peer1:');
            console.log(data);
            let type = data.split('/')[0]; //Bsp: data:text/plain;base64,... We need the part after data:
            console.log('Type1: ' + type);
            switch (type) {
                case 'data:text':
                    type = "textBASE64";
                    break;
                case 'data:image':
                    type = 'image';
                    break;
            }
            addMessageToHistory(type, data, localStorage.getItem("username"), "");
        });

    } else if (payload.type === 'answer'){
        console.log('Answer received');
        webRtcPeer.signal(payload);
    }
}

async function sendWebRtcRequest() {
    webRtcPeer = new Peer({initiator: true, tickle: false, objectMode: true});
    webRtcPeer.on('signal', function (data) {
        if (data.type === 'offer') {
            socket.emit('webRtcRequest', {'username': localStorage.getItem("username"), 'payload': JSON.stringify(data)});
        }
    });

    webRtcPeer.on('data', function (data) {
        console.log('got a message from peer2:');
        console.log(data);
        let type = data.split('/')[0]; //Bsp: data:text/plain;base64,... We need the part after data:
        console.log('Type1: ' + type);
        switch (type) {
            case 'data:text':
                type = "textBASE64";
                break;
            case 'data:image':
                type = 'image';
                break;
        }
        addMessageToHistory(type, data, localStorage.getItem("username"), "");
    });


   webRtcPeer.on('connect', function () {
        // wait for 'connect' event before using the data channel
        console.log('peer connected')
    });

    
}

async function readFile(evt) {
  var reader = new FileReader();
  var file = evt.target.files[0];

  reader.onload = (e) => {
      var data = reader.result;
      console.log(data);
              console.log('Senden...');
              webRtcPeer.send(data);

              console.log(data);
              let type = data.split('/')[0]; //Bsp: data:text/plain;base64,... We need the part after data:
              console.log('Type1: ' + type);
              switch (type) {
                  case 'data:text':
                      type = "textBASE64";
                      break;
                  case 'data:image':
                      type = 'image';
                      break;
              }
              addMessageToHistory(type, data, localStorage.getItem("username"), "");


      };

  reader.readAsDataURL(file);
}

