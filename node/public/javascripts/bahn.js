var TrainInfo = [{time: "", trainNo: "", origin: "", destination: "", track: ""},
{time: "", trainNo: "", origin: "", destination: "", track: ""},
{time: "", trainNo: "", origin: "", destination: "", track: ""},
{time: "", trainNo: "", origin: "", destination: "", track: ""},
{time: "", trainNo: "", origin: "", destination: "", track: ""}]

$(document).ready(function () {
    
});

function searchArrival(text) {
    console.log('Search')
    var formattedText = text.replace(/ /g, "+");
    //Part after "@bahn Ankunft"
    formattedText = formattedText.slice(14, formattedText.length);
    //Start Bahn API Call
    console.log('Get location of ' + formattedText);
    getLocation(formattedText, function (id) {
       
        console.log('ID in search: ' + id)
        arrivalBoard(id, function (arrivingTrains) {            
                    
            for (var i=0; i<5; i++ ){
                TrainInfo[i].trainNo = arrivingTrains[i].name;
                TrainInfo[i].origin = arrivingTrains[i].origin;
                TrainInfo[i].track = arrivingTrains[i].track;                
            }
           
            getDetails(arrivingTrains,id, function (){
                console.log(TrainInfo);
                buildMsg(function(msg){
                    sendInformation(msg, function(){
                        TrainInfo = [{time: "", trainNo: "", origin: "", destination: "", track: ""},
                        {time: "", trainNo: "", origin: "", destination: "", track: ""},
                        {time: "", trainNo: "", origin: "", destination: "", track: ""},
                        {time: "", trainNo: "", origin: "", destination: "", track: ""},
                        {time: "", trainNo: "", origin: "", destination: "", track: ""}]
                        console.log("Test")
                        console.log(TrainInfo)
                    });
                })
                //console.log(msg);
            })
        })  
    }) 
}

function buildMsg(callback){
    var msg = (TrainInfo[0].time+"   "+TrainInfo[0].trainNo+"     von "+TrainInfo[0].origin+" nach "+TrainInfo[0].destination+"   Gleis "+TrainInfo[0].track+"\n")
                msg = msg + (TrainInfo[1].time+"   "+TrainInfo[1].trainNo+"     von "+TrainInfo[1].origin+" nach "+TrainInfo[1].destination+"   Gleis "+TrainInfo[1].track+"\n")
                msg = msg + (TrainInfo[2].time+"   "+TrainInfo[2].trainNo+"     von "+TrainInfo[2].origin+" nach "+TrainInfo[2].destination+"   Gleis "+TrainInfo[2].track+"\n")
                msg = msg + (TrainInfo[3].time+"   "+TrainInfo[3].trainNo+"     von "+TrainInfo[3].origin+" nach "+TrainInfo[3].destination+"   Gleis "+TrainInfo[3].track+"\n")
                msg = msg + (TrainInfo[4].time+"   "+TrainInfo[4].trainNo+"     von "+TrainInfo[4].origin+" nach "+TrainInfo[4].destination+"   Gleis "+TrainInfo[4].track)
                
    callback(msg);
}

function sendInformation(msg, callback){
    console.log("Message that is sent to server:")
    console.log(msg)
    callback(sendMessageToServer('bahn', msg));
}

function searchDeparture(text) {
    
    console.log('Search')
    var formattedText = text.replace(/ /g, "+");
    //Part after "@bahn Ankunft"
    formattedText = formattedText.slice(14, formattedText.length);
    //Start Bahn API Call
    console.log('Get location of ' + formattedText);
    getLocation(formattedText, function (id) {
       
        console.log('ID in search: ' + id)
        arrivalBoard(id, function (departingTrains) {            
                    
            for (var i=0; i<5; i++ ){
                TrainInfo[i].trainNo = departingTrains[i].name;
                TrainInfo[i].origin = departingTrains[i].origin;
                TrainInfo[i].track = departingTrains[i].track;                
            }
           
            getDepDetails(departingTrains,id, function (){
                console.log(TrainInfo);                
                buildMsg(function(msg1){
                    sendInformation(msg1, function(){
                        TrainInfo = [{time: "", trainNo: "", origin: "", destination: "", track: ""},
                        {time: "", trainNo: "", origin: "", destination: "", track: ""},
                        {time: "", trainNo: "", origin: "", destination: "", track: ""},
                        {time: "", trainNo: "", origin: "", destination: "", track: ""},
                        {time: "", trainNo: "", origin: "", destination: "", track: ""}]
                        console.log("Test")
                        console.log(TrainInfo)
                    });
                })
            })
        })  
    }) 
}




function getDetails(arrivingTrains, id, callback) {
            
    journeyDetails(arrivingTrains[0].detailsId, function (details1) {              
       
        for (var j=0; j<details1.length; j++ ){                       
            if (details1[j].stopId == id){
                TrainInfo[0].time = details1[j].arrTime
            }                        
            if (j == (details1.length-1)){
                TrainInfo[0].destination = details1[j].stopName
            }
        }
        journeyDetails(arrivingTrains[1].detailsId, function (details1) {              
 
            for (var j=0; j<details1.length; j++ ){                       
                if (details1[j].stopId == id){
                    TrainInfo[1].time = details1[j].arrTime
                }                        
                if (j == (details1.length-1)){
                    TrainInfo[1].destination = details1[j].stopName
                }
            }
        
            journeyDetails(arrivingTrains[2].detailsId, function (details1) {              
    
                for (var j=0; j<details1.length; j++ ){                       
                    if (details1[j].stopId == id){
                        TrainInfo[2].time = details1[j].arrTime
                    }                        
                    if (j == (details1.length-1)){
                        TrainInfo[2].destination = details1[j].stopName
                    }
                }
            
                journeyDetails(arrivingTrains[3].detailsId, function (details1) {              
       
                    for (var j=0; j<details1.length; j++ ){                       
                        if (details1[j].stopId == id){
                            TrainInfo[3].time = details1[j].arrTime
                        }                        
                        if (j == (details1.length-1)){
                            TrainInfo[3].destination = details1[j].stopName
                        }
                    }
            
                    journeyDetails(arrivingTrains[4].detailsId, function (details1) {              
       
                        for (var j=0; j<details1.length; j++ ){                       
                            if (details1[j].stopId == id){
                                TrainInfo[4].time = details1[j].arrTime
                            }                        
                            if (j == (details1.length-1)){
                                TrainInfo[4].destination = details1[j].stopName
                            }
                        }
                        
                        callback()
                    }); 
                }); 
            }); 
        }); 
        
    }); 
   
    
     
    
   
}

function getDepDetails(departingTrains, id, callback) {
            
    journeyDetails(departingTrains[0].detailsId, function (details1) {              
       
        for (var j=0; j<details1.length; j++ ){                       
            if (details1[j].stopId == id){
                TrainInfo[0].time = details1[j].depTime
            }                        
            if (j == (details1.length-1)){
                TrainInfo[0].destination = details1[j].stopName
            }
        }
        journeyDetails(departingTrains[1].detailsId, function (details1) {              
 
            for (var j=0; j<details1.length; j++ ){                       
                if (details1[j].stopId == id){
                    TrainInfo[1].time = details1[j].depTime
                }                        
                if (j == (details1.length-1)){
                    TrainInfo[1].destination = details1[j].stopName
                }
            }
        
            journeyDetails(departingTrains[2].detailsId, function (details1) {              
    
                for (var j=0; j<details1.length; j++ ){                       
                    if (details1[j].stopId == id){
                        TrainInfo[2].time = details1[j].depTime
                    }                        
                    if (j == (details1.length-1)){
                        TrainInfo[2].destination = details1[j].stopName
                    }
                }
            
                journeyDetails(departingTrains[3].detailsId, function (details1) {              
       
                    for (var j=0; j<details1.length; j++ ){                       
                        if (details1[j].stopId == id){
                            TrainInfo[3].time = details1[j].depTime
                        }                        
                        if (j == (details1.length-1)){
                            TrainInfo[3].destination = details1[j].stopName
                        }
                    }
            
                    journeyDetails(departingTrains[4].detailsId, function (details1) {              
       
                        for (var j=0; j<details1.length; j++ ){                       
                            if (details1[j].stopId == id){
                                TrainInfo[4].time = details1[j].depTime
                            }                        
                            if (j == (details1.length-1)){
                                TrainInfo[4].destination = details1[j].stopName
                            }
                        }
                        
                        callback()
                    }); 
                }); 
            }); 
        });  
    });    
}


function getLocation(loc, callback) {
    var id1;
    console.log("getLocation: "+'https://api.deutschebahn.com/freeplan/v1/location/'+loc)
    $.ajax({
        url: 'https://api.deutschebahn.com/freeplan/v1/location/'+loc,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
    success: function (data, textStatus, xhr) {
        console.log('data in getLocation: ' + data)
        id1 = data[0].id
        console.log('Id One in getLoc '+ id1)
        callback(id1);
        },
        error: function (xhr, textStatus, error) {
            console.log('error:' + textStatus);
        }
    });
}

function arrivalBoard(id1, callback) {
    
    var date = new Date();
    var y = date.getFullYear();
    var m = (date.getMonth()+1).toString().padStart(2, "0");
    var dd = date.getDay().toString().padStart(2, "0");
    var hh = date.getHours().toString().padStart(2, "0");
    var mm = date.getMinutes().toString().padStart(2, "0");
    var ss = date.getSeconds().toString().padStart(2, "0");
    var time = (y+"-"+m+"-"+dd+"T"+hh+":"+mm+":"+ss)
   
    var id = id1;
    var trains = ''
    console.log(time)

    console.log("arrivalBoard: "+'https://api.deutschebahn.com/freeplan/v1/arrivalBoard/'+id+'?date='+time)
        $.ajax({
            url: 'https://api.deutschebahn.com/freeplan/v1/arrivalBoard/'+id+'?date='+time,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
        success: function (data, textStatus, xhr) {
           
            trains = data;
            callback(trains)
            },
            error: function (xhr, textStatus, error) {
                console.log('error:' + error);
            }
        });

    
    }


    function departureBoard(id1, callback) {
        var date = new Date();
        var y = date.getFullYear();
        var m = (date.getMonth()+1).toString().padStart(2, "0");
        var dd = date.getDay().toString().padStart(2, "0");
        var hh = date.getHours().toString().padStart(2, "0");
        var mm = date.getMinutes().toString().padStart(2, "0");
        var ss = date.getSeconds().toString().padStart(2, "0");
        var time = (y+"-"+m+"-"+dd+"T"+hh+":"+mm+":"+ss)
       
        var id = id1;
        var trains = ''
        console.log(time)

    console.log("arrivalBoard: "+'https://api.deutschebahn.com/freeplan/v1/departureBoard/'+id+'?date='+time)
        $.ajax({
            url: 'https://api.deutschebahn.com/freeplan/v1/departureBoard/'+id+'?date='+time,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
        success: function (data, textStatus, xhr) {
            
            trains = data          
            callback(trains)
            },
            error: function (xhr, textStatus, error) {
                console.log('error:' + textStatus);
            }
        });

    
    }


    function journeyDetails(detailsId, callback) {
    var id = encodeURI(detailsId);
    var details;
        $.ajax({
            url: 'https://api.deutschebahn.com/freeplan/v1/journeyDetails/'+id,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
        success: function (data, textStatus, xhr) {
            details = data                
            callback(details)          
            },
            error: function (xhr, textStatus, error) {
                console.log('error:' + textStatus);
            }
        });
        
    
    }


        function searchArrival1(text) {


            var promise = new Promise(function(resolve, reject) {
                console.log('Search')
                var formattedText = text.replace(/ /g, "+");
                //Part after "@bahn Ankunft"
                formattedText = formattedText.slice(14, formattedText.length);
                //Start Bahn API Call


                var id1;
                console.log("getLocation: "+'https://api.deutschebahn.com/freeplan/v1/location/'+formattedText)
                $.ajax({
                    url: 'https://api.deutschebahn.com/freeplan/v1/location/'+formattedText,
                    type: 'GET',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                success: function (data, textStatus, xhr) {
                    console.log('data in getLocation: ' + data)
                    id1 = data[0].id
                    console.log('Id One in getLoc '+ id1)
                    resolve(id1);
                    },
                    error: function (xhr, textStatus, error) {
                        console.log('error:' + textStatus);
                    }
                });  
              });
              
              promise.then(function(val) {
                var date = new Date();
                var time = date.toISOString();
                var id = val;
                var trains = ''
                console.log(time)
                console.log(id)
                console.log("arrivalBoard: "+'https://api.deutschebahn.com/freeplan/v1/arrivalBoard/'+id+'?date='+time)
                    $.ajax({
                        url: 'https://api.deutschebahn.com/freeplan/v1/arrivalBoard/'+id+'?date='+time,
                        type: 'GET',
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'json',
                    success: function (data, textStatus, xhr) {
                        console.log('arrival Board:')
                        console.log(data)
                        trains = data          
                        return trains  
                        },
                        error: function (xhr, textStatus, error) {
                            console.log('error:' + textStatus);
                        }
                    });
                              
              }).then(function(val) {
                    console.log(val); // 1
                             
              })
        }
        
