const DELETE_KEY_VALUE = 46;
const BACKSPACE_KEY_VALUE = 8;
var socket = io();
$(document).ready(function () {
    //GIPHY WEBSITE TEST
    //var xhr = $.get("http://api.giphy.com/v1/gifs/search?q=ryan+gosling&api_key=dc6zaTOxFJmzC&limit=5");
    //xhr.done(function(data) { console.log("success got data", data); });

    $('#textinput').on('keypress ', (e) => {
        var text = $('#textinput').val() + e.key;

        if (text.match("@gif")) {
            searchGif(text);
        }
    });

    $('#textinput').on('keydown', (e) => {
        var text = $('#textinput').val();
        if (e.which == DELETE_KEY_VALUE || e.which == BACKSPACE_KEY_VALUE ) {
            if (text.match("@gif")) {
                searchGif(text);
            }
        }
    });
});

function getGif(text) {
    $.ajax({
        url: '/chat/gif',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: {
            text: text
        },
    success: function (data, textStatus, xhr) {
        $('.gifView').empty();
        $('.gifView').show();
        var img;
        for (var index in data) {

            img = $("<img/>", {
                "src": data[index].img_low_quality,
                "draggable": false,
                "id": 'gif_temp',
                "alt": data[index].img_high_quality, //Wird angezeigt, wenn Browser src nicht anzeigen kann
                click: function () {
                    sendGif(this.alt);

                }
            });
            $('.gifView').append(img);

        }

        },
        error: function (xhr, textStatus, error) {
            console.log('error:' + textStatus);
        }

    });
    window.scrollTo({
        top: document.body.scrollHeight,
        behavior: "smooth"
    });
}

function searchGif(text) {
    var formattedText = text.replace(/ /g, "+");
    //Part after "@Gif"
    formattedText = formattedText.slice(5, formattedText.length);
    //Start Gif Search
    getGif(formattedText);
}

function sendGif(link) {
    sendGifToServer(link);
    $('.textarea').val('');
    $('.gifView').empty();
    $('.gifView').hide();

    window.scrollTo({
        top: document.body.scrollHeight,
        behavior: "smooth"
    });
}
