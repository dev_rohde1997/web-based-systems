var timeout_update;
var socket = io();

$(document).ready(function() {
    var timeout;
    const ENTER_KEY_VALUE = 13;
    const BACKSPACE_KEY_VALUE = 8;
    const DELETE_KEY_VALUE = 46;
    $('.textarea').focus();
    $('fa fa-spinner').hide();
    //Username
    if(localStorage.getItem("username") === null) {
        var username = window.prompt('Please enter your name', 'name ');
        localStorage.setItem("username", username);
    }
    //Load message history
    getMessageHistory();
    //Key event
    window.alert('Your are logged in as ' + localStorage.getItem("username"));




    socket.on('newMessage', (msg) => {
        getNewMessage();
    });

    $(document).on('keypress', (e) => {
        clearTimeout(timeout);
        timeout = setTimeout(function() {
            socket.emit('noLongerTpying');
            }, 2000);
        socket.emit('typing');
    });

    socket.on('typing', () => {
        $('#typinglogo').show();
    });

    socket.on('noLongerTpying', () => {
        $('#typinglogo').hide();
    });



    $('#textinput').on('keypress ', (e) => {
        switch (e.which) {
            case ENTER_KEY_VALUE :
                sendMessageToServer('text', $('#textinput').val());
                console.log('Sie haben folgendes gesendet: ' + $('#textinput').val());
                $('#textinput').val('');
                break;
        }

    });

    //interval for timestamps
    timeout_update = setTimeout(updateTimestamp, 60000);
});


function sendMessageToServer(type, text) {
    if (text.match("@bahn Ankunft")) {
        searchArrival(text)
    } else if (text.match("@bahn Abfahrt")) {
        searchDeparture(text);
    } else if(text.match("@sendfile")) {
      $('#fileInput').trigger('click');
    } else {
        console.log('sendMsgToServer: ' + text);
        $.ajax({
            url: '/chat/newMessage',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({type: type, sender: localStorage.getItem("username"), text: text})
        })
        .done(function () {
            socket.emit('newMessage', text);
            console.log('success');
        })
        .fail(function (response, textStatus) {
            console.log('error:' + textStatus);
        });
    }
}

function sendGifToServer(link) {
    $.ajax({
        url: '/chat/newMessage',
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({type: 'gif', sender: localStorage.getItem("username"), text: link})
    })
        .done(function () {
            console.log('success');
            socket.emit('newMessage', link);
        })
        .fail(function (response, textStatus) {
            console.log('error:' + textStatus);
        });
}

function getMessageHistory() {
    $.ajax({
        url: '/chat/messages',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            for (var i in data) {
                console.log(data[i]);
                addMessageToHistory(data[i].type, data[i].text, data[i].sender, data[i].timestamp);
            }
       },
        error: function (xhr, textStatus, errorThrown) {
            console.log('error:' + textStatus);
        }
    });

}


function getNewMessage() {
    $.ajax({
        url: '/chat/messages',
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (data, textStatus, xhr) {
                var len = data.length - 1;
                console.log('New message received: ' + data[len].text);
                //Neue Nachricht mit "gerade"-Timestamp
                addMessageToHistory(data[len].type, data[len].text, data[len].sender, data[len].timestamp);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log('error:' + textStatus);
        }
    });
    clearTimeout(timeout_update);
    timeout_update = setTimeout(updateTimestamp, 60000);
}

function addMessageToHistory(type, text, account, timestamp) {
    console.log('Type: ' + type);
    var d = new Date();
    var n = parseInt(d.getTime()/1000);
    var diff = n-timestamp;
    var rel_time = 0;

      if(diff<60){
        rel_time = 'gerade'
      } else {
        if (diff<3600){
            rel_time = 'vor '+parseInt(diff/60)+' Minuten';
        } else {
            if (diff<86400){
                rel_time = 'vor '+parseInt(diff/3600)+' Stunden';
            } else {
                rel_time = 'vor '+parseInt(diff/86400)+' Tagen';
            }
        }
      }
    var listElement = $("<li></li>");
    var divMsg = $("<div></div>");
    divMsg.addClass("msg");
    //Falluntertscheidung Gif oder Text
    switch (type) {
        case 'textBASE64':
            console.log('Encoding ' + text);
            text = text.split('base64,')[1];
            console.log('Decoded text: ' + text);
            text = atob(text);
        case 'text':
            var message1 = $("<p></p>").text(text);
            divMsg.append(message1);
            break;
        case 'image':
        case 'gif':
            var img = $("<img/>", {
                "src": text,
                "draggable": false,
                "id": 'gif',

            })
            divMsg.append(img);
            break;
        case 'bahn':
            let trainInfos = text.split('\n');
            console.log(trainInfos);
            for (i in trainInfos) {
                divMsg.append($("<p></p>").text(trainInfos[i]));
            }
            break;


        var time = $("<time></time>").text(rel_time);

    }
    divMsg.append(time);


    var divAvatar = $("<div></div>");
    //PATH
    const pathPart1 = "images/";
    var user;
    if (account == localStorage.getItem("username")) {
        user = "self";
    } else {
        user = "other";
    }
    const pathPart3 = ".png";
    var picturePath = pathPart1.concat(user, pathPart3);
    //PATH ENDE
    var imgAvatar = $("<img/>", {
        "src": picturePath,
        "draggable": false
    });
    divAvatar.append(imgAvatar);
    divAvatar.addClass("avatar");

    listElement.addClass(user);
    listElement.append(divAvatar);
    listElement.append(divMsg);


    $("ol.chat").append(listElement);
    window.scrollTo({
        top: document.body.scrollHeight,
        behavior: "smooth"
    });

}

//function for editing of timestamps
function updateTimestamp() {
    var d = new Date();
    var n = d.getTime();
    //Edit every timestamp
    $('time').each(function(i){
        var text = $(this).text();

        if (text.includes('gerade')){
            $(this).text('vor 1 Minuten');
        }
        if (text.includes('Minuten')){
            if (text=='vor 59 Minuten'){
                $(this).text('vor 1 Stunden');
            } else {
                var str = text.split(" ");
                var min = parseInt(str[1])+1;
                var res = str[0]+' '+min+' '+str[2];
                $(this).text(res);
            }
        }
    });
    clearTimeout(timeout_update);
    timeout_update = setTimeout(updateTimestamp, 60000);
}
